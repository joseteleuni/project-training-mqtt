#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#include <PubSubClient.h>
#include <DHT.h>
#define DHTPIN 14 //pin al que se conectara
#define DHTTYPE DHT11 //se coloca el tippo
// INSTANCIA DLE SENSOR
DHT dht(DHTPIN, DHTTYPE); // enlace a la libreria


//**************************************
//*********** MQTT CONFIG **************
//**************************************
const char *mqtt_server = "192.168.1.5"; 
const int mqtt_port = 1883; //....
const char *root_topic_subscribe = "esp32/input";
const char *root_topic_publish = "esp32/output"; 

//**************************************
//*********** WIFICONFIG ***************
//**************************************
//ip
IPAddress ip(192,168,1,153);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

//**************************************
//*********** GLOBALES   ***************
//**************************************

WiFiClient espClient;
PubSubClient client(espClient);
char msg[25];
long count=0;

int Led = 26;

//************************
//** F U N C I O N E S ***
//************************
void callback(char* topic, byte* payload, unsigned int length);
void reconnect();
void setup_wifi();

void setup() {
  Serial.begin(115200);
  pinMode(Led, OUTPUT);
  dht.begin(); // se inicia el sensor
 
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void loop() {
 float t = dht.readTemperature(); //leed datos de t° y humedad
 float h = dht.readHumidity(); //Lee la humedad
 float hic = dht.computeHeatIndex(t, h, false); // Calcular el índice de calor en grados centígrados
 String strTmp;
 char chrTmp [5];
 strTmp += t; //concatenando
 
  Serial.print("Humedad: ");
  Serial.println(h);
  Serial.println(" %\t");
  Serial.print("Temperatura: ");
  Serial.println(t); 
   Serial.print("Índice de calor: ");
  Serial.print(hic);
  Serial.print(" *C ");
   
  if (!client.connected()) {
    reconnect();
  }
  if (client.connected()){
   // String str = "La cuenta es -> " + String(count);
    strTmp.toCharArray(chrTmp,5);
    client.publish(root_topic_publish,chrTmp);
    count++;
    delay(300);
  }
  client.loop();
}

//*****************************
//***    CONEXION WIFI      ***
//*****************************
void setup_wifi(){
  delay(10);
  // Nos conectamos a nuestra red Wifi
  Serial.println();
   //IP STATICA
    WiFi.config(ip,gateway,subnet);
    //WIFI MULTI
   WiFi.mode(WIFI_STA);
   wifiMulti.addAP("ZTE", "Peru1234");
   wifiMulti.addAP("JORGEALVITEZ_Ext", "70985166_Jl");
   wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3");

  while (wifiMulti.run() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("Conectado a red WiFi!");
  Serial.println("Dirección IP: ");
  Serial.println(WiFi.localIP());
}

//*****************************
//***    CONEXION MQTT      ***
//*****************************

void reconnect() {

  while (!client.connected()) {
    Serial.print("Intentando conexión Mqtt...");
    // Creamos un cliente ID
    String clientId = "IOTICOS_H_W_";
    clientId += String(random(0xffff), HEX);
    // Intentamos conectar
    if (client.connect("root_topic_subscribe")) {
      Serial.println("Conectado!");
      // Nos suscribimos
      if(client.subscribe(root_topic_subscribe)){
        Serial.println("Suscripcion ok");
      }else{
        Serial.println("fallo Suscripciión");
      }
    } else {
      Serial.print("falló :( con error -> ");
      Serial.print(client.state());
      Serial.println(" Intentamos de nuevo en 5 segundos");
      delay(5000);
    }
  }
}

//*****************************
//***       CALLBACK        ***
//*****************************

void callback(char* topic, byte* payload, unsigned int length){
  String incoming = "";
  Serial.print("Mensaje recibido desde -> ");
  Serial.print(topic);
  Serial.println("");
    for (int i = 0; i < length; i++) {
    incoming += (char)payload[i];
    
  }
   if (incoming == "1") {
    Serial.println("Encender Foco");
    digitalWrite(Led, HIGH);
  } else if(incoming == "0") {
    Serial.println("Apagar Foco");
    digitalWrite(Led, LOW);
  } 
 incoming.trim();
 Serial.println("Mensaje -> " + incoming);
}
