import paho.mqtt.client as mqtt
import time

def ConectadoMQTT(client, userdata, flags, rc):
    print("Connectado"+str(rc))
    client.subscribe("/Boton")

def MensajeMQTT(client, userdata, msg):
     if msg.topic == "esp32":
       print (f"ESP32 : {str(msg.payload)}")

client = mqtt.Client()
client.on_connect = ConectadoMQTT
client.on_message = MensajeMQTT

#client.username_pw_set("mosquitto", "peru123")
client.connect("voip.winaytel.com", 1883, 60)

#apagar y encender led en 5 segundos
client.publish("LED", "Apagar")
time.sleep(5)
client.publish("LED", "Encender")

client.loop_forever()

