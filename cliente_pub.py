import paho.mqtt.client as mqtt
import lib_platform as platf
import json
from datetime import datetime
from datetime import date
import psutil


cliente = mqtt.Client("Edificio Tower")
cliente.connect("voip.winaytel.com",1883,60)
#cliente.publish("edificio/temperatura","{foco01:20 , foco02:21 , foco3:19}")
#mensaje=platform.linux_distribution()

t_sistema    = platf.system
h_sistema    = platf.hostname
c_cpu        = psutil.cpu_percent(interval = None)
r_ram        = psutil.virtual_memory().used / (1024.0 ** 3)

r_ram =round(r_ram,2)

now          = datetime.now()
current_time = str(now.strftime("%H:%M:%S"))
hoy          = str(date.today())

if c_cpu < 5:
    consumo = 1
elif c_cpu > 5 and c_cpu < 10:
    consumo = 2
else:
  consumo = 3


mensaje={'sistema':t_sistema , 'hostname':h_sistema ,'fecha': hoy,'hora':current_time , 'cpu':c_cpu , 'ram':r_ram,'estado_cpu':consumo}
cliente.publish("edificio/temperatura",str(mensaje))
